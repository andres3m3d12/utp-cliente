from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'  # Configura la URL de conexión a la base de datos SQLite
db = SQLAlchemy(app)

# Definición de un modelo de ejemplo
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True)

    def __init__(self, username):
        self.username = username

# Ruta para crear un nuevo usuario en la base de datos
@app.route('/user/create/<username>')
def create_user(username):
    user = User(username)
    db.session.add(user)
    db.session.commit()
    return 'Usuario creado con éxito'

# Ruta para obtener todos los usuarios de la base de datos
@app.route('/users')
def get_users():
    users = User.query.all()
    result = ''
    for user in users:
        result += user.username + '<br>'
    return result

if __name__ == '__main__':
    app.run()
En este ejemplo, hemos utilizado la extensión Flask-SQLAlchemy para simplificar la interacción con la base de datos. Hemos configurado la URL de conexión a una base de datos SQLite llamada database.db. Luego, hemos definido un modelo de datos User que representa una tabla en la base de datos.

La ruta /user/create/<username> permite crear un nuevo usuario en la base de datos. Se crea una instancia del modelo User con el nombre de usuario proporcionado, se agrega a la sesión de la base de datos y se realiza la confirmación para guardar los cambios



